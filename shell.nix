{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
	buildInputs = with pkgs; [
		libisoburn
		mtools
		nasm
		perl
		qemu
		zig

		(writeShellScriptBin "qemu-system-x86_64-uefi" ''
			'${qemu}/bin/qemu-system-x86_64' -bios ${OVMF.fd}/FV/OVMF.fd "$@"
		'')
	];
}
