const std = @import("std");

const Target = enum {
    x86_64_uefi,

    pub fn getTargetQuery(self: Target) std.Target.Query {
        return switch (self) {
            Target.x86_64_uefi => std.Target.Query{
                .cpu_arch = std.Target.Cpu.Arch.x86_64,
                .os_tag = std.Target.Os.Tag.uefi,
                .abi = std.Target.Abi.msvc,
            },
        };
    }

    pub fn getAsm(self: Target, b: *std.Build) std.Build.LazyPath {
        switch (self) {
            Target.x86_64_uefi => {
                const asm_src = b.addSystemCommand(&.{ "nasm", "-f", "win64", "-o" });
                const asm_o = asm_src.addOutputFileArg("asm.o");
                asm_src.addFileArg(b.path("src/arch/x86_64-uefi/asm.asm"));
                return asm_o;
            },
        }
    }
};

fn getTarget(b: *std.Build) Target {
    const target_ = b.option([]const u8, "target", "Target to build kernel for");
    const target_default = Target.x86_64_uefi;

    if (target_) |target| {
        if (std.mem.eql(u8, target, "x86_64-uefi")) {
            return Target.x86_64_uefi;
        } else {
            std.log.err("Unknown target: {s}", .{target});
            b.invalid_user_input = true;
            return target_default;
        }
    } else {
        return target_default;
    }
}

pub fn build(b: *std.Build) void {
    const target = getTarget(b);
    const optimize = b.standardOptimizeOption(std.Build.StandardOptimizeOptionOptions{});

    // Kernel
    const kernel = b.addExecutable(std.Build.ExecutableOptions{
        .name = "learnkernel1",
        .root_source_file = b.path("src/main.zig"),
        .target = b.resolveTargetQuery(target.getTargetQuery()),
        .optimize = optimize,
    });
    kernel.addObjectFile(target.getAsm(b));

    const options = b.addOptions();
    options.addOption(Target, "target", target);
    kernel.root_module.addOptions("config", options);

    const kernel_install = b.addInstallArtifact(kernel, .{ .dest_dir = .{ .override = .prefix } });
    b.getInstallStep().dependOn(&kernel_install.step);

    // Create IMG
    const img = b.step("img", "Build a disk image");
    const img_cmd = b.addSystemCommand(&.{"perl"});
    img_cmd.addFileArg(b.path("./scripts/generate_img.pl"));
    img_cmd.addArg("--kernel");
    img_cmd.addFileArg(kernel.getEmittedBin());
    img_cmd.addArg("--output");
    const img_path = img_cmd.addOutputFileArg("disk.img");
    const img_install = b.addInstallFileWithDir(img_path, .prefix, "disk.img");
    img.dependOn(&img_install.step);
    b.getInstallStep().dependOn(&img_install.step);

    // Create ISO
    const iso = b.step("iso", "Build an ISO image");
    const iso_cmd = b.addSystemCommand(&.{"perl"});
    iso_cmd.addFileArg(b.path("./scripts/generate_iso.pl"));
    iso_cmd.addArg("--image");
    iso_cmd.addFileArg(img_path);
    iso_cmd.addArg("--output");
    const iso_path = iso_cmd.addOutputFileArg("disk.iso");
    const iso_install = b.addInstallFileWithDir(iso_path, .prefix, "disk.iso");
    iso.dependOn(&iso_install.step);
    b.getInstallStep().dependOn(&iso_install.step);

    // Run
    const run = b.step("run", "Run kernel");
    const run_cmd = b.addSystemCommand(&.{ "qemu-system-x86_64-uefi", "-machine", "q35", "-debugcon", "stdio", "-device", "ide-cd,drive=cdrom", "-blockdev" });
    run_cmd.addPrefixedFileArg("driver=file,node-name=cdrom,filename=", iso_path);
    run.dependOn(&run_cmd.step);

    // Unit tests
    const test_step = b.step("test", "Run unit tests");
    const test_src = b.addTest(std.Build.TestOptions{
        .root_source_file = b.path("src/main.zig"),
        .target = b.resolveTargetQuery(std.Target.Query{}),
        .optimize = optimize,
    });
    const test_run = b.addRunArtifact(test_src);
    test_step.dependOn(&test_run.step);
}
