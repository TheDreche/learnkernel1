const config = @import("config");

const Main = switch (config.target) {
    .x86_64_uefi => @import("arch/x86_64-uefi/main.zig"),
};

pub const main = Main.prepare;
