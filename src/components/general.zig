// This component is special as it does not depend on any platform specific
// code and thus should not be platform specific. However, the contents are
// actually platform specific, but present on so many platforms that porting to
// a different platform would probably require another programming language
// either way.

const std = @import("std");

pub const Bit = u1;
pub const Byte = u8;
pub const bits_per_byte = 8;
pub const ByteBitIdentifier = u3;

pub fn byteGetBit(self: Byte, number: ByteBitIdentifier) Bit {
    return @truncate(self >> number);
}

comptime {
    std.debug.assert(@sizeOf(Byte) == 1);
    std.debug.assert(bits_per_byte == std.mem.byte_size_in_bits);
}
