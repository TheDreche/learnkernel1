const std = @import("std");

const platform = @import("../platform.zig").interface;

fn myWrite(_: void, msg: []const u8) error{}!usize {
    for (msg) |c| {
        platform.dbgPutChar(c);
    }
    return msg.len;
}

pub const Writer = std.io.Writer(void, error{}, myWrite);
pub const writer = Writer{
    .context = {},
};

pub fn print(msg: []const u8) void {
    writer.writeAll(msg) catch |e| switch (e) {};
}

pub fn printf(comptime format: []const u8, args: anytype) void {
    writer.print(format, args) catch |e| switch (e) {};
}

pub const assert = std.debug.assert;

pub fn breakpoint() void {
    while (true) {}
}
