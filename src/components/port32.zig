const platform = @import("../platform.zig").interface;

pub const Port = struct {
    number: platform.Port32,

    pub fn read(self: Port) u32 {
        return platform.port32Read(self.number);
    }
    pub fn write(self: Port, value: u32) void {
        platform.port32Write(self.number, value);
    }
};
