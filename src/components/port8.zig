const platform = @import("../platform.zig").interface;

const Byte = @import("general.zig").Byte;

pub const Port = struct {
    number: platform.PortByte,

    pub fn read(self: Port) Byte {
        return platform.portByteRead(self.number);
    }
    pub fn write(self: Port, value: Byte) void {
        platform.portByteWrite(self.number, value);
    }
};
