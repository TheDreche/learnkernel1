const std = @import("std");
const types = @import("../types.zig");

const general = @import("general.zig");
const dbg = @import("dbg.zig");
const port32 = @import("port32.zig");

pub const Rsdp = packed struct {
    signature: types.ArrayPacked(u8, 8),
    checksum: general.Byte,
    oemid: types.ArrayPacked(u8, 6),
    revision: u8,
    rsdtAddress: u32,
    length: u32,
    xsdtAddress: u64,
    extendedChecksum: general.Byte,
    reserved: types.ArrayPacked(u8, 6),
};
comptime {
    std.debug.assert(@offsetOf(Rsdp, "signature") == 0);
    std.debug.assert(@offsetOf(Rsdp, "checksum") == 8);
    std.debug.assert(@offsetOf(Rsdp, "oemid") == 9);
    std.debug.assert(@offsetOf(Rsdp, "revision") == 15);
    std.debug.assert(@offsetOf(Rsdp, "rsdtAddress") == 16);
    std.debug.assert(@offsetOf(Rsdp, "length") == 20);
    std.debug.assert(@offsetOf(Rsdp, "xsdtAddress") == 24);
    std.debug.assert(@offsetOf(Rsdp, "extendedChecksum") == 32);
    std.debug.assert(@offsetOf(Rsdp, "reserved") == 33);
}
pub fn getXsdt(rsdp: *allowzero align(1) Rsdp) XsdtP {
    return @ptrFromInt(rsdp.xsdtAddress);
}

pub const DescriptionHeader = packed struct {
    signature: types.ArrayPacked(u8, 4),
    length: u32,
    revision: u8,
    checksum: general.Byte,
    oem_id: types.ArrayPacked(u8, 6),
    oem_table_id: types.ArrayPacked(u8, 8),
    oem_revision: u32,
    creator_id: u32,
    creator_revision: u32,

    const size = 36;

    pub fn check(self: *allowzero align(1) DescriptionHeader) bool {
        const bytes = @as([*]allowzero align(1) general.Byte, @ptrCast(self))[0..self.length];
        var sum: general.Byte = 0;
        for (bytes) |byte| {
            sum +%= byte;
        }
        return sum == 0;
    }
};
comptime {
    std.debug.assert(@offsetOf(DescriptionHeader, "signature") == 0);
    std.debug.assert(@offsetOf(DescriptionHeader, "length") == 4);
    std.debug.assert(@offsetOf(DescriptionHeader, "revision") == 8);
    std.debug.assert(@offsetOf(DescriptionHeader, "checksum") == 9);
    std.debug.assert(@offsetOf(DescriptionHeader, "oem_id") == 10);
    std.debug.assert(@offsetOf(DescriptionHeader, "oem_table_id") == 16);
    std.debug.assert(@offsetOf(DescriptionHeader, "oem_revision") == 24);
    std.debug.assert(@offsetOf(DescriptionHeader, "creator_id") == 28);
    std.debug.assert(@offsetOf(DescriptionHeader, "creator_revision") == 32);
}

pub const GenericAddress = packed struct {
    pub const Space = enum(u8) {
        system_memory = 0x00,
        system_io = 0x01,
        pci_configuration = 0x02,
        embedded_controller = 0x03,
        smbus = 0x04,
        system_cmos = 0x05,
        pci_bar_target = 0x06,
        ipmi = 0x07,
        general_purpose_io = 0x08,
        generic_serial_bus = 0x09,
        platform_communications_channel = 0x0A,
        platform_runtime_mechanism = 0x0B,
        // reserved,
        functional_fixed_hardware = 0x7F,
        // OEM defined values,
    };

    pub const AccessSize = enum(u8) {
        undefined_ = 0,
        byte = 1,
        word = 2,
        dword = 3,
        qword = 4,
    };

    space_id: Space,
    bit_width: u8,
    bit_offset: u8,
    access_size: AccessSize,
    address: u64,
};
comptime {
    std.debug.assert(@offsetOf(GenericAddress, "space_id") == 0);
    std.debug.assert(@offsetOf(GenericAddress, "bit_width") == 1);
    std.debug.assert(@offsetOf(GenericAddress, "bit_offset") == 2);
    std.debug.assert(@offsetOf(GenericAddress, "access_size") == 3);
    std.debug.assert(@offsetOf(GenericAddress, "address") == 4);
}

pub const XsdtP = *allowzero align(1) Xsdt;
pub const Xsdt = packed struct {
    header: DescriptionHeader,

    pub fn getTable(self: XsdtP) []align(1) *allowzero align(1) DescriptionHeader {
        return @as([*]align(1) *allowzero align(1) DescriptionHeader, @ptrFromInt(@intFromPtr(&self.header) + DescriptionHeader.size))[0 .. (self.header.length - DescriptionHeader.size) / 8];
    }

    pub fn findTable(self: XsdtP, name: [4]u8) ?*allowzero align(1) DescriptionHeader {
        for (self.getTable()) |table| {
            std.debug.assert(table.check());
            if (std.mem.eql(u8, &table.signature.getArray(), &name)) {
                return table;
            }
        }
        return null;
    }

    pub fn findFadt(self: XsdtP) FadtP {
        // Why is there a difference?
        //return @fieldParentPtr("header", self.findTable("FACP".*).?);
        return @ptrCast(self.findTable("FACP".*).?);
    }

    pub fn findMcfg(self: XsdtP) McfgP {
        //return @fieldParentPtr("header", self.findTable("MCFG".*).?);
        return @ptrCast(self.findTable("MCFG".*).?);
    }
};
comptime {
    std.debug.assert(@offsetOf(Xsdt, "header") == 0);
}

pub const FadtP = *allowzero align(1) Fadt;
pub const Fadt = packed struct {
    pub const PreferredPowerManagementProfile = enum(general.Byte) {
        unspecified = 0,
        desktop = 1,
        mobile = 2,
        workstation = 3,
        server_enterprise = 4,
        server_soho = 5,
        pc_appliance = 6,
        server_performance = 7,
        tablet = 8,
    };

    header: DescriptionHeader,
    firmware_ctrl: u32,
    dsdt: u32,
    reserved0: general.Byte,
    preferred_power_management_profile: PreferredPowerManagementProfile,
    sci_int: u16,
    smi_cmd: u32,
    acpi_enable: u8,
    acpi_disable: u8,
    s4bios_req: u8,
    pstate_cnt: u8,
    pm1a_evt_blk: u32,
    pm1b_evt_blk: u32,
    pm1a_cnt_blk: u32,
    pm1b_cnt_blk: u32,
    pm2_cnt_blk: u32,
    pm_tmr_blk: u32,
    gpe0_blk: u32,
    gpe1_blk: u32,
    pm1_evt_len: u8,
    pm1_cnt_len: u8,
    pm2_cnt_len: u8,
    pm_tmr_len: u8,
    gpe0_blk_len: u8,
    gpe1_blk_len: u8,
    gpe1_base: u8,
    cst_cnt: u8,
    p_lvl2_lat: u16,
    p_lvl3_lat: u16,
    flush_size: u16,
    flush_strude: u16,
    duty_offset: u8,
    duty_width: u8,
    day_alarm: u8,
    mon_alarm: u8,
    century: u8,
    iapc_boot_arch: u16,
    reserved1: general.Byte,
    flags: u32,
    reset_reg: GenericAddress,
    reset_value: u8,
    arm_boot_arch: u16,
    fadt_minor: u8,
    x_firmware_control: u64,
    x_dsdt: u64,
    x_pm1a_evt_blk: GenericAddress,
    x_pm1b_evt_blk: GenericAddress,
    x_pm1a_cnt_blk: GenericAddress,
    x_pm1b_cnt_blk: GenericAddress,
    x_pm2_cnt_blk: GenericAddress,
    x_pm_tmr_blk: GenericAddress,
    x_gpe0_blk: GenericAddress,
    x_gpe1_blk: GenericAddress,
    sleep_control_reg: GenericAddress,
    sleep_status_reg: GenericAddress,
    hypervisor_vendor_identity: u64,
};
comptime {
    std.debug.assert(@offsetOf(Fadt, "header") == 0);
}

pub const McfgP = *allowzero align(1) Mcfg;
pub const Mcfg = packed struct {
    header: DescriptionHeader,

    pub const ConfigurationSpaceBaseAddress = packed struct {
        base_addr: *anyopaque,
        segment: u16,
        bus_start: u8,
        bus_end: u8,
        reserved: u32,
    };
    comptime {
        std.debug.assert(@offsetOf(ConfigurationSpaceBaseAddress, "base_addr") == 0);
        std.debug.assert(@offsetOf(ConfigurationSpaceBaseAddress, "segment") == 8);
        std.debug.assert(@offsetOf(ConfigurationSpaceBaseAddress, "bus_start") == 10);
        std.debug.assert(@offsetOf(ConfigurationSpaceBaseAddress, "bus_end") == 11);
        std.debug.assert(@sizeOf(ConfigurationSpaceBaseAddress) == 16);
    }

    pub fn getConfigurationSpaceBaseAddresses(self: McfgP) []align(1) ConfigurationSpaceBaseAddress {
        return @as([*]align(1) ConfigurationSpaceBaseAddress, @ptrFromInt(@intFromPtr(self) + 44))[0..@divExact(self.header.length - 44, 16)];
    }
};
comptime {
    std.debug.assert(@offsetOf(Mcfg, "header") == 0);
}
pub const AcpiTables = struct {
    xsdt: XsdtP,
    fadt: FadtP,
    mcfg: McfgP,
};

pub fn init(acpi_xsdt: XsdtP) AcpiTables {
    dbg.print("Searching several ACPI tables\n");
    for (acpi_xsdt.getTable()) |header| {
        dbg.printf("{s}\n", .{header.signature.getArray()});
        std.debug.assert(header.check());
    }
    const acpi_fadt: FadtP = acpi_xsdt.findFadt();
    const acpi_mcfg: McfgP = acpi_xsdt.findMcfg();
    dbg.printf("XSDT: {*}\nFADT: {*}\nMCFG: {*}\n", .{ acpi_xsdt, acpi_fadt, acpi_mcfg });

    dbg.printf("Port {x} enable {x} disable {x}\n", .{ acpi_fadt.smi_cmd, acpi_fadt.acpi_enable, acpi_fadt.acpi_disable });

    return AcpiTables{
        .xsdt = acpi_xsdt,
        .fadt = acpi_fadt,
        .mcfg = acpi_mcfg,
    };
}

pub fn setup(tables: AcpiTables) void {
    const acpi_fadt = tables.fadt;
    if (acpi_fadt.smi_cmd != 0 and acpi_fadt.acpi_enable != 0 and acpi_fadt.acpi_disable != 0) {
        // TODO: Read PM1a control block (or rather PM1 control grouping?)
        (port32.Port{ .number = @intCast(acpi_fadt.smi_cmd) }).write(acpi_fadt.acpi_enable);
    }
    // TODO: Enable if necessary
}
