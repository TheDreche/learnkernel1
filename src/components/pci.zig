const std = @import("std");

const acpi = @import("acpi.zig");
const port32 = @import("port32.zig");
const dbg = @import("dbg.zig");

pub const Address = struct {
    const portAddress = port32.Port{ .number = 0xCF8 };
    const portData = port32.Port{ .number = 0xCFC };

    address: usize,

    // TODO: readU16, readU32 (also converting from Little Endian)
    pub fn readDWord(address: Address) u32 {
        std.debug.assert(address.address & 0b11 == 0);
        return @atomicLoad(u32, @as(*align(4) u32, @ptrFromInt(address.address)), std.builtin.AtomicOrder.unordered);
    }

    pub fn writeDWord(address: Address, value: u32) void {
        std.debug.assert(address.address & 0b11 == 0);
        @atomicStore(u32, @as(*align(4) u32, @ptrFromInt(address.address)), value, std.builtin.AtomicOrder.unordered);
    }

    pub fn readWord(address: Address) u16 {
        std.debug.assert(address.address & 0b1 == 0);

        return @truncate((Address{
            .address = address.address & ~@as(usize, 0b11),
        }).readDWord() >> @intCast(8 * (address.address & 0b11)));
    }

    pub fn readByte(address: Address) u8 {
        return @truncate((Address{
            .address = address.address & ~@as(usize, 0b11),
        }).readDWord() >> @intCast(8 * (address.address & 0b11)));
    }

    pub const readU8 = readByte;
};

pub const Function = struct {
    base: usize,

    pub fn atOffset(function: Function, offset: u12) Address {
        return Address{ .address = function.base + offset };
    }

    pub fn getVendorId(function: Function) u16 {
        return function.atOffset(0x00).readWord();
    }
    pub fn getDeviceId(function: Function) u16 {
        return function.atOffset(0x02).readWord();
    }
    pub const HeaderType = enum(u7) {
        general = 0x0,
        pci2pci = 0x1,
        pci2cardbus = 0x2,
    };
    pub fn getHeaderType(function: Function) HeaderType {
        return @enumFromInt(function.atOffset(0x0E).readByte() & 0x7F);
    }
    pub fn isMultiFunctional(function: Function) bool {
        return (function.atOffset(0x0E).readByte() & 0x80) != 0;
    }
    pub fn getClass(function: Function) u16 {
        return function.atOffset(0x0A).readWord();
    }
};

const Init = struct {
    fn checkFunction(mcfg: acpi.Mcfg.ConfigurationSpaceBaseAddress, function: Function) void {
        dbg.printf("{x:0>16}: [{x:0>4}] [{x:0>4}:{x:0>4}]\n", .{
            function.base,
            function.getClass(),
            function.getVendorId(),
            function.getDeviceId(),
        });
        dbg.assert((function.getClass() == 0x0406) == (function.getHeaderType() == Function.HeaderType.pci2pci));

        const class = function.getClass();
        switch (class) {
            0x0406 => checkBus(mcfg, function.atOffset(0x19).readByte()),
            else => {},
        }
    }
    fn checkDevice(mcfg: acpi.Mcfg.ConfigurationSpaceBaseAddress, base: usize) void {
        var function = Function{ .base = base };
        const vendor_id = function.getVendorId();
        if (vendor_id == 0xFFFF) {
            return;
        }

        if (function.isMultiFunctional()) {
            for (0..std.math.maxInt(u3) + 1) |function_number_| {
                const function_number: u3 = @intCast(function_number_);
                function.base = base + (@as(usize, function_number) << 12);
                if (function.getVendorId() != 0xFFFF) {
                    checkFunction(mcfg, function);
                }
            }
        } else {
            checkFunction(mcfg, function);
        }
    }
    fn checkBus(mcfg: acpi.Mcfg.ConfigurationSpaceBaseAddress, bus: u8) void {
        for (0..std.math.maxInt(u5) + 1) |device_| {
            const device: u5 = @intCast(device_);
            checkDevice(mcfg, @intFromPtr(mcfg.base_addr) + ((@as(usize, bus) << 20) + (@as(usize, device) << 15)));
        }
    }
};
pub fn init(acpi_mcfg: acpi.McfgP) void {
    { // Enumerate all PCI buses
        dbg.print("Enumerating PCI...\n");
        for (acpi_mcfg.getConfigurationSpaceBaseAddresses()) |mcfg| {
            std.debug.assert((@intFromPtr(mcfg.base_addr) & 0b11) == 0); // Does this need to be the case? (It is also assumed in upper asserts)
            Init.checkBus(mcfg, mcfg.bus_start);
        }
    }
}
