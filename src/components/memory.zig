// TODO: Thread safety (for allocation and deallocation)
// TODO: Higher half kernel (removing all allowzero)
const std = @import("std");
const pages = @import("page.zig");

pub const PagePointer = *allowzero align(pages.page_size) pages.Page;

const PageUnallocated = struct {
    next: ?*allowzero align(pages.page_size) PageUnallocated,
};

var unallocated_first: ?*allowzero align(pages.page_size) PageUnallocated = null;

pub const AllocationError = error{
    OutOfMemory,
};

// Allocates a single page (the by far most frequent use case)
pub fn alloc() AllocationError!*allowzero align(pages.page_size) pages.Page {
    if (unallocated_first) |first| {
        unallocated_first = first.next;
        return @ptrCast(first);
    } else {
        return AllocationError.OutOfMemory;
    }
}

// Frees a single page
pub fn free(page: PagePointer) void {
    const page_unallocated: *align(pages.page_size) PageUnallocated = @ptrCast(page);
    page_unallocated.next = unallocated_first;
    unallocated_first = page_unallocated;
}
