const std = @import("std");

const platform = @import("../platform.zig");

const general = @import("general.zig");

pub const page_size = platform.page_size;
pub const Page = [page_size]general.Byte;
pub const PageP = *align(page_size) Page;

comptime {
    std.debug.assert(@sizeOf(Page) == page_size);
}

pub fn pageOf(address: *anyopaque) PageP {
    return @ptrFromInt(@divFloor(@intFromPtr(address), page_size));
}
