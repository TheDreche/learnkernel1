const assembly = @import("asm.zig");
const dbg = @import("dbg.zig");
const descriptor = @import("descriptor.zig");
const tss = @import("tss.zig");

const Gdt = packed struct {
    dNull: descriptor.Null,
    dKernelCode: descriptor.SegmentCode,
    dKernelData: descriptor.SegmentData,
    dUserCode: descriptor.SegmentCode,
    dUserData: descriptor.SegmentData,
    dTask: descriptor.Tss,
};

var gdt align(16) = Gdt{
    .dNull = descriptor.Null.init(), // 0x00
    .dKernelCode = descriptor.SegmentCode.init(true, false, 0), // 0x08
    .dKernelData = descriptor.SegmentData.init(true, false, 0), // 0x10
    .dUserCode = descriptor.SegmentCode.init(true, false, 3), // 0x18
    .dUserData = descriptor.SegmentData.init(true, false, 3), // 0x20
    .dTask = undefined, // 0x28
};

pub const KERNEL_CODE = 0x08;
pub const KERNEL_DATA = 0x10;
pub const USER_CODE = 0x18;
pub const USER_DATA = 0x20;
pub const TASK = 0x28;

pub fn init() void {
    dbg.print("Setting up GDT\n");
    gdt.dTask = descriptor.Tss.init(&tss.tss, @sizeOf(tss.TaskStateSegment) - 1, 0, true);
    var pointer = assembly.GDTPointer{
        .base = &gdt,
        .limit = @sizeOf(Gdt) - 1,
    };
    assembly.lgdt(&pointer, KERNEL_CODE, KERNEL_DATA, TASK);
}
