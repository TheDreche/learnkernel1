; vim:filetype=nasm:

section .text

;;;;;;;;;;;
; asm.zig ;
;;;;;;;;;;;

global asmLgdt

asmLgdt:
	lgdt [rcx]
	mov rax, R9
	ltr ax
	mov rax, R8
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	pop rax
	push rdx
	push rax
	retfq

global asmStackSwitch

; asmStackSwitch(data, stack, function) noreturn
;
; 1. Use the given stack
; 2. Call function with the same parameters; MUST NOT RETURN
asmStackSwitch:
	mov rsp, rcx
	sub rsp, 40 ; Required for calling convention
	jmp r8 ; Call function; since it will never return, the return address is not needed

;;;;;;;;;;;;;;
; Interrupts ;
;;;;;;;;;;;;;;

global asmIgnoreInterrupt
asmIgnoreInterrupt:
	iretq

%macro asmHandleInterrupt 1
extern handleInterrupt%1
global asmHandleInterrupt%1
asmHandleInterrupt%1:
	sub rsp, 0x8 ; Padding (for 16 byte alignment)

	push rax
	push rbx
	push rcx
	push rdx
	push rsi
	push rdi
	; Skip rsp
	push rbp
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15

	; Not needed (for now)
	;push gs
	;push fs
	;push es
	;push ds
	sub rsp, 8
	; Stack is now re-aligned to 16 bytes

	sub rsp, 0x100
	movdqa [rsp + 0x00], xmm15
	movdqa [rsp + 0x10], xmm14
	movdqa [rsp + 0x20], xmm13
	movdqa [rsp + 0x30], xmm12
	movdqa [rsp + 0x40], xmm11
	movdqa [rsp + 0x50], xmm10
	movdqa [rsp + 0x60], xmm9
	movdqa [rsp + 0x70], xmm8
	movdqa [rsp + 0x80], xmm7
	movdqa [rsp + 0x90], xmm6
	movdqa [rsp + 0xA0], xmm5
	movdqa [rsp + 0xB0], xmm4
	movdqa [rsp + 0xC0], xmm3
	movdqa [rsp + 0xD0], xmm2
	movdqa [rsp + 0xE0], xmm1
	movdqa [rsp + 0xF0], xmm0

	mov rcx, rsp
	add rcx, 0x188
	mov rdx, rsp
	sub rsp, 0x20 ; Required for calling convention
	call handleInterrupt%1

	movdqa [rsp + 0x020], xmm15
	movdqa [rsp + 0x030], xmm14
	movdqa [rsp + 0x040], xmm13
	movdqa [rsp + 0x050], xmm12
	movdqa [rsp + 0x060], xmm11
	movdqa [rsp + 0x070], xmm10
	movdqa [rsp + 0x080], xmm9
	movdqa [rsp + 0x090], xmm8
	movdqa [rsp + 0x0A0], xmm7
	movdqa [rsp + 0x0B0], xmm6
	movdqa [rsp + 0x0C0], xmm5
	movdqa [rsp + 0x0D0], xmm4
	movdqa [rsp + 0x0E0], xmm3
	movdqa [rsp + 0x0F0], xmm2
	movdqa [rsp + 0x100], xmm1
	movdqa [rsp + 0x110], xmm0
	add rsp, 0x128

	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop rbp
	; rsp
	pop rdi
	pop rsi
	pop rdx
	pop rcx
	pop rbx
	pop rax

	add rsp, 0x8
	iretq
%endmacro

%macro asmHandleInterruptErr 1
extern handleInterrupt%1
global asmHandleInterrupt%1
asmHandleInterrupt%1:
	; Stack already aligned on 16 bytes

	push rax
	push rbx
	push rcx
	push rdx
	push rsi
	push rdi
	; Skip rsp
	push rbp
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	push r15

	; Not needed (for now)
	;push gs
	;push fs
	;push es
	;push ds
	sub rsp, 8
	; Stack is now re-aligned to 16 bytes


	sub rsp, 0x100
	movdqa [rsp + 0x00], xmm15
	movdqa [rsp + 0x10], xmm14
	movdqa [rsp + 0x20], xmm13
	movdqa [rsp + 0x30], xmm12
	movdqa [rsp + 0x40], xmm11
	movdqa [rsp + 0x50], xmm10
	movdqa [rsp + 0x60], xmm9
	movdqa [rsp + 0x70], xmm8
	movdqa [rsp + 0x80], xmm7
	movdqa [rsp + 0x90], xmm6
	movdqa [rsp + 0xA0], xmm5
	movdqa [rsp + 0xB0], xmm4
	movdqa [rsp + 0xC0], xmm3
	movdqa [rsp + 0xD0], xmm2
	movdqa [rsp + 0xE0], xmm1
	movdqa [rsp + 0xF0], xmm0

	mov rcx, rsp
	add rcx, 0x188
	mov rdx, rsp
	mov r8, [rsp + 0x180]
	sub rsp, 0x20 ; Required for calling convention
	call handleInterrupt%1

	movdqa [rsp + 0x020], xmm15
	movdqa [rsp + 0x030], xmm14
	movdqa [rsp + 0x040], xmm13
	movdqa [rsp + 0x050], xmm12
	movdqa [rsp + 0x060], xmm11
	movdqa [rsp + 0x070], xmm10
	movdqa [rsp + 0x080], xmm9
	movdqa [rsp + 0x090], xmm8
	movdqa [rsp + 0x0A0], xmm7
	movdqa [rsp + 0x0B0], xmm6
	movdqa [rsp + 0x0C0], xmm5
	movdqa [rsp + 0x0D0], xmm4
	movdqa [rsp + 0x0E0], xmm3
	movdqa [rsp + 0x0F0], xmm2
	movdqa [rsp + 0x100], xmm1
	movdqa [rsp + 0x110], xmm0
	add rsp, 0x128

	pop r15
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop rbp
	; rsp
	pop rdi
	pop rsi
	pop rdx
	pop rcx
	pop rbx
	pop rax

	add rsp, 8 ; Discard error code
	iretq
%endmacro

asmHandleInterruptErr 8
asmHandleInterrupt 48
asmHandleInterrupt 128
