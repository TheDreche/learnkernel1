const assembly = @import("asm.zig");
const descriptor = @import("descriptor.zig");
const dbg = @import("dbg.zig");
const gdt = @import("gdt.zig");
const isr = @import("isr.zig");

var table align(16) = assembly.IDT{
    descriptor.IdtDescriptor.initNull(), // 0x00
    descriptor.IdtDescriptor.initNull(), // 0x01
    descriptor.IdtDescriptor.initNull(), // 0x02
    descriptor.IdtDescriptor.initNull(), // 0x03
    descriptor.IdtDescriptor.initNull(), // 0x04
    descriptor.IdtDescriptor.initNull(), // 0x05
    descriptor.IdtDescriptor.initNull(), // 0x06
    descriptor.IdtDescriptor.initNull(), // 0x07
    descriptor.IdtDescriptor.initNull(), // 0x08
    descriptor.IdtDescriptor.initNull(), // 0x09
    descriptor.IdtDescriptor.initNull(), // 0x0A
    descriptor.IdtDescriptor.initNull(), // 0x0B
    descriptor.IdtDescriptor.initNull(), // 0x0C
    descriptor.IdtDescriptor.initNull(), // 0x0D
    descriptor.IdtDescriptor.initNull(), // 0x0E
    descriptor.IdtDescriptor.initNull(), // 0x0F
    descriptor.IdtDescriptor.initNull(), // 0x10
    descriptor.IdtDescriptor.initNull(), // 0x11
    descriptor.IdtDescriptor.initNull(), // 0x12
    descriptor.IdtDescriptor.initNull(), // 0x13
    descriptor.IdtDescriptor.initNull(), // 0x14
    descriptor.IdtDescriptor.initNull(), // 0x15
    descriptor.IdtDescriptor.initNull(), // 0x16
    descriptor.IdtDescriptor.initNull(), // 0x17
    descriptor.IdtDescriptor.initNull(), // 0x18
    descriptor.IdtDescriptor.initNull(), // 0x19
    descriptor.IdtDescriptor.initNull(), // 0x1A
    descriptor.IdtDescriptor.initNull(), // 0x1B
    descriptor.IdtDescriptor.initNull(), // 0x1C
    descriptor.IdtDescriptor.initNull(), // 0x1D
    descriptor.IdtDescriptor.initNull(), // 0x1E
    descriptor.IdtDescriptor.initNull(), // 0x1F
    descriptor.IdtDescriptor.initNull(), // 0x20
    descriptor.IdtDescriptor.initNull(), // 0x21
    descriptor.IdtDescriptor.initNull(), // 0x22
    descriptor.IdtDescriptor.initNull(), // 0x23
    descriptor.IdtDescriptor.initNull(), // 0x24
    descriptor.IdtDescriptor.initNull(), // 0x25
    descriptor.IdtDescriptor.initNull(), // 0x26
    descriptor.IdtDescriptor.initNull(), // 0x27
    descriptor.IdtDescriptor.initNull(), // 0x28
    descriptor.IdtDescriptor.initNull(), // 0x29
    descriptor.IdtDescriptor.initNull(), // 0x2A
    descriptor.IdtDescriptor.initNull(), // 0x2B
    descriptor.IdtDescriptor.initNull(), // 0x2C
    descriptor.IdtDescriptor.initNull(), // 0x2D
    descriptor.IdtDescriptor.initNull(), // 0x2E
    descriptor.IdtDescriptor.initNull(), // 0x2F
    descriptor.IdtDescriptor.initNull(), // 0x30
    descriptor.IdtDescriptor.initNull(), // 0x31
    descriptor.IdtDescriptor.initNull(), // 0x32
    descriptor.IdtDescriptor.initNull(), // 0x33
    descriptor.IdtDescriptor.initNull(), // 0x34
    descriptor.IdtDescriptor.initNull(), // 0x35
    descriptor.IdtDescriptor.initNull(), // 0x36
    descriptor.IdtDescriptor.initNull(), // 0x37
    descriptor.IdtDescriptor.initNull(), // 0x38
    descriptor.IdtDescriptor.initNull(), // 0x39
    descriptor.IdtDescriptor.initNull(), // 0x3A
    descriptor.IdtDescriptor.initNull(), // 0x3B
    descriptor.IdtDescriptor.initNull(), // 0x3C
    descriptor.IdtDescriptor.initNull(), // 0x3D
    descriptor.IdtDescriptor.initNull(), // 0x3E
    descriptor.IdtDescriptor.initNull(), // 0x3F
    descriptor.IdtDescriptor.initNull(), // 0x40
    descriptor.IdtDescriptor.initNull(), // 0x41
    descriptor.IdtDescriptor.initNull(), // 0x42
    descriptor.IdtDescriptor.initNull(), // 0x43
    descriptor.IdtDescriptor.initNull(), // 0x44
    descriptor.IdtDescriptor.initNull(), // 0x45
    descriptor.IdtDescriptor.initNull(), // 0x46
    descriptor.IdtDescriptor.initNull(), // 0x47
    descriptor.IdtDescriptor.initNull(), // 0x48
    descriptor.IdtDescriptor.initNull(), // 0x49
    descriptor.IdtDescriptor.initNull(), // 0x4A
    descriptor.IdtDescriptor.initNull(), // 0x4B
    descriptor.IdtDescriptor.initNull(), // 0x4C
    descriptor.IdtDescriptor.initNull(), // 0x4D
    descriptor.IdtDescriptor.initNull(), // 0x4E
    descriptor.IdtDescriptor.initNull(), // 0x4F
    descriptor.IdtDescriptor.initNull(), // 0x50
    descriptor.IdtDescriptor.initNull(), // 0x51
    descriptor.IdtDescriptor.initNull(), // 0x52
    descriptor.IdtDescriptor.initNull(), // 0x53
    descriptor.IdtDescriptor.initNull(), // 0x54
    descriptor.IdtDescriptor.initNull(), // 0x55
    descriptor.IdtDescriptor.initNull(), // 0x56
    descriptor.IdtDescriptor.initNull(), // 0x57
    descriptor.IdtDescriptor.initNull(), // 0x58
    descriptor.IdtDescriptor.initNull(), // 0x59
    descriptor.IdtDescriptor.initNull(), // 0x5A
    descriptor.IdtDescriptor.initNull(), // 0x5B
    descriptor.IdtDescriptor.initNull(), // 0x5C
    descriptor.IdtDescriptor.initNull(), // 0x5D
    descriptor.IdtDescriptor.initNull(), // 0x5E
    descriptor.IdtDescriptor.initNull(), // 0x5F
    descriptor.IdtDescriptor.initNull(), // 0x60
    descriptor.IdtDescriptor.initNull(), // 0x61
    descriptor.IdtDescriptor.initNull(), // 0x62
    descriptor.IdtDescriptor.initNull(), // 0x63
    descriptor.IdtDescriptor.initNull(), // 0x64
    descriptor.IdtDescriptor.initNull(), // 0x65
    descriptor.IdtDescriptor.initNull(), // 0x66
    descriptor.IdtDescriptor.initNull(), // 0x67
    descriptor.IdtDescriptor.initNull(), // 0x68
    descriptor.IdtDescriptor.initNull(), // 0x69
    descriptor.IdtDescriptor.initNull(), // 0x6A
    descriptor.IdtDescriptor.initNull(), // 0x6B
    descriptor.IdtDescriptor.initNull(), // 0x6C
    descriptor.IdtDescriptor.initNull(), // 0x6D
    descriptor.IdtDescriptor.initNull(), // 0x6E
    descriptor.IdtDescriptor.initNull(), // 0x6F
    descriptor.IdtDescriptor.initNull(), // 0x70
    descriptor.IdtDescriptor.initNull(), // 0x71
    descriptor.IdtDescriptor.initNull(), // 0x72
    descriptor.IdtDescriptor.initNull(), // 0x73
    descriptor.IdtDescriptor.initNull(), // 0x74
    descriptor.IdtDescriptor.initNull(), // 0x75
    descriptor.IdtDescriptor.initNull(), // 0x76
    descriptor.IdtDescriptor.initNull(), // 0x77
    descriptor.IdtDescriptor.initNull(), // 0x78
    descriptor.IdtDescriptor.initNull(), // 0x79
    descriptor.IdtDescriptor.initNull(), // 0x7A
    descriptor.IdtDescriptor.initNull(), // 0x7B
    descriptor.IdtDescriptor.initNull(), // 0x7C
    descriptor.IdtDescriptor.initNull(), // 0x7D
    descriptor.IdtDescriptor.initNull(), // 0x7E
    descriptor.IdtDescriptor.initNull(), // 0x7F
    descriptor.IdtDescriptor.initNull(), // 0x80
    descriptor.IdtDescriptor.initNull(), // 0x81
    descriptor.IdtDescriptor.initNull(), // 0x82
    descriptor.IdtDescriptor.initNull(), // 0x83
    descriptor.IdtDescriptor.initNull(), // 0x84
    descriptor.IdtDescriptor.initNull(), // 0x85
    descriptor.IdtDescriptor.initNull(), // 0x86
    descriptor.IdtDescriptor.initNull(), // 0x87
    descriptor.IdtDescriptor.initNull(), // 0x88
    descriptor.IdtDescriptor.initNull(), // 0x89
    descriptor.IdtDescriptor.initNull(), // 0x8A
    descriptor.IdtDescriptor.initNull(), // 0x8B
    descriptor.IdtDescriptor.initNull(), // 0x8C
    descriptor.IdtDescriptor.initNull(), // 0x8D
    descriptor.IdtDescriptor.initNull(), // 0x8E
    descriptor.IdtDescriptor.initNull(), // 0x8F
    descriptor.IdtDescriptor.initNull(), // 0x90
    descriptor.IdtDescriptor.initNull(), // 0x91
    descriptor.IdtDescriptor.initNull(), // 0x92
    descriptor.IdtDescriptor.initNull(), // 0x93
    descriptor.IdtDescriptor.initNull(), // 0x94
    descriptor.IdtDescriptor.initNull(), // 0x95
    descriptor.IdtDescriptor.initNull(), // 0x96
    descriptor.IdtDescriptor.initNull(), // 0x97
    descriptor.IdtDescriptor.initNull(), // 0x98
    descriptor.IdtDescriptor.initNull(), // 0x99
    descriptor.IdtDescriptor.initNull(), // 0x9A
    descriptor.IdtDescriptor.initNull(), // 0x9B
    descriptor.IdtDescriptor.initNull(), // 0x9C
    descriptor.IdtDescriptor.initNull(), // 0x9D
    descriptor.IdtDescriptor.initNull(), // 0x9E
    descriptor.IdtDescriptor.initNull(), // 0x9F
    descriptor.IdtDescriptor.initNull(), // 0xA0
    descriptor.IdtDescriptor.initNull(), // 0xA1
    descriptor.IdtDescriptor.initNull(), // 0xA2
    descriptor.IdtDescriptor.initNull(), // 0xA3
    descriptor.IdtDescriptor.initNull(), // 0xA4
    descriptor.IdtDescriptor.initNull(), // 0xA5
    descriptor.IdtDescriptor.initNull(), // 0xA6
    descriptor.IdtDescriptor.initNull(), // 0xA7
    descriptor.IdtDescriptor.initNull(), // 0xA8
    descriptor.IdtDescriptor.initNull(), // 0xA9
    descriptor.IdtDescriptor.initNull(), // 0xAA
    descriptor.IdtDescriptor.initNull(), // 0xAB
    descriptor.IdtDescriptor.initNull(), // 0xAC
    descriptor.IdtDescriptor.initNull(), // 0xAD
    descriptor.IdtDescriptor.initNull(), // 0xAE
    descriptor.IdtDescriptor.initNull(), // 0xAF
    descriptor.IdtDescriptor.initNull(), // 0xB0
    descriptor.IdtDescriptor.initNull(), // 0xB1
    descriptor.IdtDescriptor.initNull(), // 0xB2
    descriptor.IdtDescriptor.initNull(), // 0xB3
    descriptor.IdtDescriptor.initNull(), // 0xB4
    descriptor.IdtDescriptor.initNull(), // 0xB5
    descriptor.IdtDescriptor.initNull(), // 0xB6
    descriptor.IdtDescriptor.initNull(), // 0xB7
    descriptor.IdtDescriptor.initNull(), // 0xB8
    descriptor.IdtDescriptor.initNull(), // 0xB9
    descriptor.IdtDescriptor.initNull(), // 0xBA
    descriptor.IdtDescriptor.initNull(), // 0xBB
    descriptor.IdtDescriptor.initNull(), // 0xBC
    descriptor.IdtDescriptor.initNull(), // 0xBD
    descriptor.IdtDescriptor.initNull(), // 0xBE
    descriptor.IdtDescriptor.initNull(), // 0xBF
    descriptor.IdtDescriptor.initNull(), // 0xC0
    descriptor.IdtDescriptor.initNull(), // 0xC1
    descriptor.IdtDescriptor.initNull(), // 0xC2
    descriptor.IdtDescriptor.initNull(), // 0xC3
    descriptor.IdtDescriptor.initNull(), // 0xC4
    descriptor.IdtDescriptor.initNull(), // 0xC5
    descriptor.IdtDescriptor.initNull(), // 0xC6
    descriptor.IdtDescriptor.initNull(), // 0xC7
    descriptor.IdtDescriptor.initNull(), // 0xC8
    descriptor.IdtDescriptor.initNull(), // 0xC9
    descriptor.IdtDescriptor.initNull(), // 0xCA
    descriptor.IdtDescriptor.initNull(), // 0xCB
    descriptor.IdtDescriptor.initNull(), // 0xCC
    descriptor.IdtDescriptor.initNull(), // 0xCD
    descriptor.IdtDescriptor.initNull(), // 0xCE
    descriptor.IdtDescriptor.initNull(), // 0xCF
    descriptor.IdtDescriptor.initNull(), // 0xD0
    descriptor.IdtDescriptor.initNull(), // 0xD1
    descriptor.IdtDescriptor.initNull(), // 0xD2
    descriptor.IdtDescriptor.initNull(), // 0xD3
    descriptor.IdtDescriptor.initNull(), // 0xD4
    descriptor.IdtDescriptor.initNull(), // 0xD5
    descriptor.IdtDescriptor.initNull(), // 0xD6
    descriptor.IdtDescriptor.initNull(), // 0xD7
    descriptor.IdtDescriptor.initNull(), // 0xD8
    descriptor.IdtDescriptor.initNull(), // 0xD9
    descriptor.IdtDescriptor.initNull(), // 0xDA
    descriptor.IdtDescriptor.initNull(), // 0xDB
    descriptor.IdtDescriptor.initNull(), // 0xDC
    descriptor.IdtDescriptor.initNull(), // 0xDD
    descriptor.IdtDescriptor.initNull(), // 0xDE
    descriptor.IdtDescriptor.initNull(), // 0xDF
    descriptor.IdtDescriptor.initNull(), // 0xE0
    descriptor.IdtDescriptor.initNull(), // 0xE1
    descriptor.IdtDescriptor.initNull(), // 0xE2
    descriptor.IdtDescriptor.initNull(), // 0xE3
    descriptor.IdtDescriptor.initNull(), // 0xE4
    descriptor.IdtDescriptor.initNull(), // 0xE5
    descriptor.IdtDescriptor.initNull(), // 0xE6
    descriptor.IdtDescriptor.initNull(), // 0xE7
    descriptor.IdtDescriptor.initNull(), // 0xE8
    descriptor.IdtDescriptor.initNull(), // 0xE9
    descriptor.IdtDescriptor.initNull(), // 0xEA
    descriptor.IdtDescriptor.initNull(), // 0xEB
    descriptor.IdtDescriptor.initNull(), // 0xEC
    descriptor.IdtDescriptor.initNull(), // 0xED
    descriptor.IdtDescriptor.initNull(), // 0xEE
    descriptor.IdtDescriptor.initNull(), // 0xEF
    descriptor.IdtDescriptor.initNull(), // 0xF0
    descriptor.IdtDescriptor.initNull(), // 0xF1
    descriptor.IdtDescriptor.initNull(), // 0xF2
    descriptor.IdtDescriptor.initNull(), // 0xF3
    descriptor.IdtDescriptor.initNull(), // 0xF4
    descriptor.IdtDescriptor.initNull(), // 0xF5
    descriptor.IdtDescriptor.initNull(), // 0xF6
    descriptor.IdtDescriptor.initNull(), // 0xF7
    descriptor.IdtDescriptor.initNull(), // 0xF8
    descriptor.IdtDescriptor.initNull(), // 0xF9
    descriptor.IdtDescriptor.initNull(), // 0xFA
    descriptor.IdtDescriptor.initNull(), // 0xFB
    descriptor.IdtDescriptor.initNull(), // 0xFC
    descriptor.IdtDescriptor.initNull(), // 0xFD
    descriptor.IdtDescriptor.initNull(), // 0xFE
    descriptor.IdtDescriptor.initNull(), // 0xFF
};

fn setHandler(vector: usize, handler: descriptor.IdtDescriptor) void {
    table[vector] = handler;
}

extern fn asmIgnoreInterrupt() callconv(.Interrupt) void;
extern fn asmHandleInterrupt8() callconv(.Interrupt) void;
extern fn asmHandleInterrupt48() callconv(.Interrupt) void;
extern fn asmHandleInterrupt128() callconv(.Interrupt) void;
pub fn init() void {
    isr.noop(); // Required for having zig compile the exported functions
    dbg.print("Setting up IDT\n");
    setHandler(0x08, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmHandleInterrupt8, gdt.KERNEL_CODE, 1, 0) });

    // PIC interrupts are still in IDT in case the mask should not be accepted for some reason
    setHandler(0x20, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmIgnoreInterrupt, gdt.KERNEL_CODE, 0, 0) });
    setHandler(0x21, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmIgnoreInterrupt, gdt.KERNEL_CODE, 0, 0) });
    setHandler(0x22, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmIgnoreInterrupt, gdt.KERNEL_CODE, 0, 0) });
    setHandler(0x23, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmIgnoreInterrupt, gdt.KERNEL_CODE, 0, 0) });
    setHandler(0x24, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmIgnoreInterrupt, gdt.KERNEL_CODE, 0, 0) });
    setHandler(0x25, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmIgnoreInterrupt, gdt.KERNEL_CODE, 0, 0) });
    setHandler(0x26, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmIgnoreInterrupt, gdt.KERNEL_CODE, 0, 0) });
    setHandler(0x27, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmIgnoreInterrupt, gdt.KERNEL_CODE, 0, 0) });

    setHandler(0x30, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmHandleInterrupt48, gdt.KERNEL_CODE, 0, 0) }); // Timer
    setHandler(0x80, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmHandleInterrupt128, gdt.KERNEL_CODE, 0, 0) });
    setHandler(0xFF, descriptor.IdtDescriptor{ .interrupt64 = descriptor.Interrupt64.init(&asmIgnoreInterrupt, gdt.KERNEL_CODE, 0, 0) }); // Spurious APIC interrupt

    assembly.lidt(&table);
    assembly.sti();
}
