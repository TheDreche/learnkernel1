const assembly = @import("asm.zig");
const descriptor = @import("descriptor.zig");

pub const TaskStateSegment = packed struct {
    reserved0: u32,
    rsp0: u64,
    rsp1: u64,
    rsp2: u64,
    reserved1: u64,
    ist1: u64,
    ist2: u64,
    ist3: u64,
    ist4: u64,
    ist5: u64,
    ist6: u64,
    ist7: u64,
    reserved2: u64,
    reserved3: u16,
    iobp_offset: u16,
};

pub var tss: TaskStateSegment align(assembly.page_size) = TaskStateSegment{
    .reserved0 = 0,
    .rsp0 = 0,
    .rsp1 = 0,
    .rsp2 = 0,
    .reserved1 = 0,
    .ist1 = 0,
    .ist2 = 0,
    .ist3 = 0,
    .ist4 = 0,
    .ist5 = 0,
    .ist6 = 0,
    .ist7 = 0,
    .reserved2 = 0,
    .reserved3 = 0,
    .iobp_offset = 0,
};

pub fn setPrivilegeStack(privilege: descriptor.Privilege, stack: *assembly.Stack) void {
    const stack_ = @intFromPtr(stack);
    switch (privilege) {
        0 => tss.rsp0 = stack_,
        1 => tss.rsp1 = stack_,
        2 => tss.rsp2 = stack_,
        3 => unreachable,
    }
}

pub fn setIstStack(entry: u3, stack: *assembly.Stack) void {
    const stack_ = @intFromPtr(stack);
    switch (entry) {
        0 => unreachable,
        1 => tss.ist1 = stack_,
        2 => tss.ist2 = stack_,
        3 => tss.ist3 = stack_,
        4 => tss.ist4 = stack_,
        5 => tss.ist5 = stack_,
        6 => tss.ist6 = stack_,
        7 => tss.ist7 = stack_,
    }
}
