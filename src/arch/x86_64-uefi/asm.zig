const descriptor = @import("descriptor.zig");

// Memory
pub const bits_per_byte = 8;
pub const Byte = u8;

pub const Word = u16;
pub const WordDouble = u32;
pub const WordQuadruple = u64;

pub const AddressPhysical = u52; // Maximum supported
pub const address_physical_bits_max = 52;

pub const page_zeros = 12;
pub const page_size: usize = 1 << page_zeros;
pub const Page = [page_size]Byte;

pub const Stack = opaque {};

pub fn pageStack(page: *allowzero align(page_size) Page) *Stack {
    return @ptrCast(@as([*]allowzero align(page_size) Page, @ptrCast(page)) + 1);
}

extern fn asmStackSwitch(data: *anyopaque, stack: *Stack, callback: *const fn (data: *anyopaque) callconv(.C) noreturn) noreturn;

pub const stackSwitch = asmStackSwitch;

// Global Descriptor Table
pub const GDTPointer = packed struct {
    limit: u16,
    base: *allowzero const anyopaque,
};

extern fn asmLgdt(gdt: *GDTPointer, segment_code: u16, segment_data: u16, segment_task: u16) void;
pub const lgdt = asmLgdt;

// Interrupts
pub fn cli() void {
    asm volatile ("cli");
}

pub fn sti() void {
    asm volatile ("sti");
}

pub const IDT = [256]descriptor.IdtDescriptor;

const IDTPointer = packed struct {
    limit: u16,
    base: *allowzero align(16) const IDT,
};

pub fn lidt(table: *allowzero align(16) const IDT) void {
    const pointer = IDTPointer{
        .limit = @sizeOf(IDT) - 1,
        .base = table,
    };
    asm volatile ("lidt (%[idt])"
        :
        : [idt] "r" (&pointer),
    );
}
