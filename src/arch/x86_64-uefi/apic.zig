const std = @import("std");

const port8 = @import("../../components/port8.zig");

const assembly = @import("asm.zig");
const dbg = @import("dbg.zig");
const cpuid = @import("cpuid.zig");
const msr = @import("msr.zig");

const Address = assembly.AddressPhysical;

var base: assembly.AddressPhysical = undefined;

fn set(offset: Address, value: u32) void {
    asm volatile ("movl %[in], (%[out])"
        :
        : [in] "r" (value),
          [out] "r" (base + offset),
    );
}

fn get(offset: Address) u32 {
    return asm ("movl (%[address]), %[out]"
        : [out] "r" (-> u32),
        : [address] "r" (base + offset),
    );
}

fn getBase(features: cpuid.Features) void {
    base = @truncate(msr.get(0x1B) & ~(@as(assembly.AddressPhysical, 1 << assembly.page_zeros) - 1) & ((@as(assembly.AddressPhysical, 1) << @intCast(features.physicalAddressSize)) - 1));
}

fn setBase(features: cpuid.Features, address: assembly.AddressPhysical) void {
    std.debug.assert(address % assembly.page_size == 0);

    const bitmap: u64 = ~(((1 << features.physicalAddressSize) - 1) << 12);
    const data = msr.get(0x1B);
    msr.set(0x1B, (data & bitmap) | (address << 12));
}

fn setSpurious(vector: u8, enable: bool, focus_check: bool) void {
    set(0xF0, vector | ((if (enable) @as(u32, 1) else 0) << 8) | ((if (focus_check) @as(u32, 1) else 0) << 9));
}

fn setTimer(vector: u8, periodic: bool, interval: u32) void {
    set(0x320, vector | ((if (periodic) @as(u32, 1) else 0) << 17));
    set(0x380, interval);
}

const DivideValue = enum(u3) {
    d001 = 0b111,
    d002 = 0b000,
    d004 = 0b001,
    d008 = 0b010,
    d016 = 0b011,
    d032 = 0b100,
    d064 = 0b101,
    d128 = 0b110,
};

fn setTimerDivide(divide_value: DivideValue) void {
    set(0x3E0, (@intFromEnum(divide_value) & 0b011) | (@intFromEnum(divide_value) & 0b100) << 1);
}

pub fn init(features: cpuid.Features) void {
    dbg.print("Setting up APIC\n");

    // Disable x2APIC, enable APIC (usually not necessary)
    msr.set(0x1B, (msr.get(0x1B) & ~@as(u64, 1 << 10)) | (1 << 11));

    getBase(features);
    dbg.printf("APIC base: {x}\n", .{base});
    set(0x280, 0); // Clear errors
    setSpurious(0xFF, true, false);
    setTimerDivide(DivideValue.d001);
    setTimer(0x30, false, 0xFFFF);

    // Disable 8259 PIC
    const pic_master_command = port8.Port{ .number = 0x20 };
    const pic_master_data = port8.Port{ .number = 0x21 };
    const pic_slave_command = port8.Port{ .number = 0xA0 };
    const pic_slave_data = port8.Port{ .number = 0xA1 };

    pic_master_command.write(0x11);
    pic_slave_command.write(0x11);
    pic_master_data.write(0x20);
    pic_slave_data.write(0x20);
    pic_master_data.write(4);
    pic_slave_data.write(2);
    pic_master_data.write(1);
    pic_slave_data.write(1);
    pic_master_data.write(0xFF);
    pic_slave_data.write(0xFF);
}

pub fn eoi() void {
    set(0xB0, 0);
}
