const std = @import("std");
const uefi = std.os.uefi;

// apic != acpi
const apic = @import("apic.zig");
const assembly = @import("asm.zig");
const cpuid = @import("cpuid.zig");
const dbg = @import("dbg.zig");
const gdt = @import("gdt.zig");
const idt = @import("idt.zig");
const msr = @import("msr.zig");
const tss = @import("tss.zig");

const acpi = @import("../../components/acpi.zig");
const memory = @import("../../components/memory.zig");
const pci = @import("../../components/pci.zig");

const EarlyBootError = error{
    OutOfMemory,
    UnsuccessfulFirmwareCall,
    ProtocolMissingLoadedImage,
    ProtocolMissingPci,
    // cpuid
    UnknownVendor,
    MissingFeature,
    AddressPhysicalTooLong,
    // acpi
    AcpiNoRsdp,
};

fn earlyBootPanic(out: *uefi.protocol.SimpleTextOutput, e: EarlyBootError) void {
    _ = switch (e) {
        EarlyBootError.OutOfMemory => out.outputString(&[_:0]u16{ 'C', 'o', 'u', 'l', 'd', ' ', 'n', 'o', 't', ' ', 'a', 'l', 'l', 'o', 'c', 'a', 't', 'e', ' ', 'm', 'e', 'm', 'o', 'r', 'y', '!' }),
        EarlyBootError.UnsuccessfulFirmwareCall => out.outputString(&[_:0]u16{ 'T', 'h', 'e', ' ', 'f', 'i', 'r', 'm', 'w', 'a', 'r', 'e', ' ', 'r', 'e', 'p', 'o', 'r', 't', 'e', 'd', ' ', 'a', 'n', ' ', 'e', 'r', 'r', 'o', 'r', '.' }),
        EarlyBootError.ProtocolMissingLoadedImage => out.outputString(&[_:0]u16{ 'C', 'o', 'u', 'l', 'd', ' ', 'n', 'o', 't', ' ', 'l', 'o', 'c', 'a', 't', 'e', ' ', 'E', 'F', 'I', ' ', 'L', 'o', 'a', 'd', 'e', 'd', 'I', 'm', 'a', 'g', 'e', '!' }),
        EarlyBootError.ProtocolMissingPci => out.outputString(&[_:0]u16{ 'C', 'o', 'u', 'l', 'd', ' ', 'n', 'o', 't', ' ', 'l', 'o', 'c', 'a', 't', 'e', ' ', 'E', 'F', 'I', ' ', 'P', 'c', 'i', 'R', 'o', 'o', 't', 'B', 'r', 'i', 'd', 'g', 'e', 'I', 'o', '!' }),
        EarlyBootError.UnknownVendor => out.outputString(&[_:0]u16{ 'U', 'n', 'k', 'n', 'o', 'w', 'n', ' ', 'C', 'P', 'U', ' ', 'v', 'e', 'n', 'd', 'o', 'r', '!' }),
        EarlyBootError.MissingFeature => out.outputString(&[_:0]u16{ 'M', 'i', 's', 's', 'i', 'n', 'g', ' ', 'C', 'P', 'U', ' ', 'f', 'e', 'a', 't', 'u', 'r', 'e', '!' }),
        EarlyBootError.AddressPhysicalTooLong => out.outputString(&[_:0]u16{ 'P', 'h', 'y', 's', 'i', 'c', 'a', 'l', ' ', 'a', 'd', 'd', 'r', 'e', 's', 's', ' ', 's', 'i', 'z', 'e', ' ', 't', 'o', 'o', ' ', 'l', 'o', 'n', 'g', '!' }),
        EarlyBootError.AcpiNoRsdp => out.outputString(&[_:0]u16{ 'C', 'o', 'u', 'l', 'd', ' ', 'n', 'o', 't', ' ', 'l', 'o', 'c', 'a', 't', 'e', ' ', 'A', 'C', 'P', 'I', ' ', 'R', 'S', 'D', 'P', '!' }),
    };
    _ = uefi.system_table.boot_services.?.stall(5000000);
}

const UefiMemoryMapPointer = struct {
    address: usize,
    count: usize,
    descriptor_size: usize,

    fn getEntry(self: UefiMemoryMapPointer, number: usize) *uefi.tables.MemoryDescriptor {
        return @ptrFromInt(self.address + number * self.descriptor_size);
    }
};

const InitialMemorySetup = struct {
    memory_map: UefiMemoryMapPointer,
    kernel: []allowzero align(assembly.page_size) assembly.Page,
    stack: *allowzero align(assembly.page_size) assembly.Page,
    features: cpuid.Features,
    xsdt: acpi.XsdtP,
};

pub fn prepare() void {
    if (uefi.system_table.boot_services == null) {
        return;
    }
    const simple_text = initUefiText() catch {
        _ = uefi.system_table.boot_services.?.stall(2000000);
        return;
    };
    const features = cpuid.init(simple_text) catch |err| {
        earlyBootPanic(simple_text, switch (err) {
            cpuid.Error.UnsuccessfulFirmwareCall => EarlyBootError.UnsuccessfulFirmwareCall,
            cpuid.Error.MissingFeature => EarlyBootError.MissingFeature,
            cpuid.Error.UnknownVendor => EarlyBootError.UnknownVendor,
            cpuid.Error.AddressPhysicalTooLong => EarlyBootError.AddressPhysicalTooLong,
        });
        return;
    };
    checkPci() catch |err| {
        earlyBootPanic(simple_text, err);
        return;
    };
    const xsdt = locateAcpi() catch |err| {
        earlyBootPanic(simple_text, err);
        return;
    };
    const kernel_pages = locateImage() catch |err| {
        earlyBootPanic(simple_text, err);
        return;
    };
    const stack: *allowzero align(assembly.page_size) assembly.Page = allocateStack() catch |err| {
        earlyBootPanic(simple_text, err);
        return;
    };
    const memory_map = exitBootServices() catch |err| {
        earlyBootPanic(simple_text, err);
        return;
    };
    assembly.cli();

    var initial_memory_setup = InitialMemorySetup{
        .memory_map = memory_map,
        .kernel = kernel_pages,
        .stack = stack,
        .features = features,
        .xsdt = xsdt,
    };
    assembly.stackSwitch(&initial_memory_setup, assembly.pageStack(stack), @ptrCast(&boot));
}

fn boot(initial_memory_setup: *allowzero InitialMemorySetup) callconv(.C) noreturn {
    // Copy to our stack (so that the old stack can be used as available memory)
    const memory_map = initial_memory_setup.memory_map;
    const kernel_pages = initial_memory_setup.kernel;
    const stack = initial_memory_setup.stack;
    const features = initial_memory_setup.features;
    const acpi_xsdt = initial_memory_setup.xsdt;

    // Initialize systems
    gdt.init();
    idt.init();
    initAllocator(memory_map, kernel_pages, stack);
    apic.init(features);

    dbg.print("Setting up IDT stacks\n");
    tss.setPrivilegeStack(0, assembly.pageStack(memory.alloc() catch |err| switch (err) {
        memory.AllocationError.OutOfMemory => {
            dbg.print("Failed to allocate page!\n");
            dbg.breakpoint();
            while (true) {}
        },
    }));
    tss.setIstStack(1, assembly.pageStack(memory.alloc() catch |err| switch (err) {
        memory.AllocationError.OutOfMemory => {
            dbg.print("Failed to allocate page!\n");
            dbg.breakpoint();
            while (true) {}
        },
    }));

    const acpi_tables = acpi.init(acpi_xsdt);
    pci.init(acpi_tables.mcfg);
    acpi.setup(acpi_tables);

    dbg.print("Done\n");

    var v = msr.get(0xC0000080);
    dbg.printf("MSRv {x}\n", .{v});
    msr.set(0xC0000080, v);
    v = msr.get(0xC0000080);
    dbg.printf("MSRv {x}\n", .{v});

    dbg.print("After\n");
    while (true) {}
}

// UEFI step procedures
fn initUefiText() !*uefi.protocol.SimpleTextOutput {
    if (uefi.system_table.con_out) |simple_text| {
        if (uefi.Status.Success != simple_text.reset(false)) {
            return EarlyBootError.UnsuccessfulFirmwareCall;
        }
        if (uefi.Status.Success != simple_text.enableCursor(true)) {
            return EarlyBootError.UnsuccessfulFirmwareCall;
        }
        if (uefi.Status.Success != simple_text.outputString(&[_:0]u16{ 'H', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd', '!', '\r', '\n' })) {
            return EarlyBootError.UnsuccessfulFirmwareCall;
        }
        return simple_text;
    } else {
        return error.ProtocolMissingSimpleText;
    }
}

fn locateAcpi() EarlyBootError!acpi.XsdtP {
    var iter = uefi.system_table.configuration_table;
    while (!iter[0].vendor_guid.eql(uefi.tables.ConfigurationTable.acpi_20_table_guid)) { // TODO: Break on end of configuration table and return error instead
        iter += 1;
    }
    return acpi.getXsdt(@ptrCast(iter[0].vendor_table));
}

fn checkPci() EarlyBootError!void {
    var pci_protocol: ?*anyopaque = undefined;
    const pci_protocol_guid: uefi.Guid align(8) = uefi.Guid{
        .time_low = 0x2F707EBB,
        .time_mid = 0x4A1A,
        .time_high_and_version = 0x11d4,
        .clock_seq_high_and_reserved = 0x9A,
        .clock_seq_low = 0x38,
        .node = [_]u8{ 0x00, 0x90, 0x27, 0x3F, 0xC1, 0x4D },
    };
    switch (uefi.system_table.boot_services.?.locateProtocol(&pci_protocol_guid, null, &pci_protocol)) {
        uefi.Status.Success => {},
        uefi.Status.InvalidParameter => unreachable,
        uefi.Status.NotFound => {
            // If the protocol is missing, it is *very* likely that PCI is not supported
            return EarlyBootError.ProtocolMissingPci;
        },
        else => unreachable,
    }
}

fn locateImage() EarlyBootError![]allowzero align(assembly.page_size) assembly.Page {
    var loadedImageProtocol: ?*uefi.protocol.LoadedImage = null;
    switch (uefi.system_table.boot_services.?.handleProtocol(uefi.handle, &uefi.protocol.LoadedImage.guid, @ptrCast(&loadedImageProtocol))) {
        uefi.Status.Success => if (loadedImageProtocol) |loadedImage| {
            const v = bytesPages(loadedImage.image_base[0..loadedImage.image_size]);
            return v;
        } else {
            unreachable;
        },
        uefi.Status.Unsupported => return EarlyBootError.ProtocolMissingLoadedImage,
        else => return EarlyBootError.UnsuccessfulFirmwareCall,
    }
}

fn allocateStack() EarlyBootError!*allowzero align(assembly.page_size) assembly.Page {
    var page: *allowzero align(assembly.page_size) assembly.Page = undefined;
    return switch (uefi.system_table.boot_services.?.allocatePages(uefi.tables.AllocateType.AllocateAnyPages, uefi.tables.MemoryType.LoaderData, 1, @ptrCast(&page))) {
        uefi.Status.Success => page,
        uefi.Status.OutOfResources => EarlyBootError.OutOfMemory,
        else => unreachable,
    };
}

fn exitBootServices() EarlyBootError!UefiMemoryMapPointer {
    // Get memory map
    var memory_map_size: usize = 0;
    var memory_map: [*]uefi.tables.MemoryDescriptor = undefined;
    var memory_map_key: usize = undefined;
    var memory_map_descriptor_size: usize = undefined;
    var memory_map_descriptor_version: u32 = undefined;
    while (uefi.Status.BufferTooSmall == uefi.system_table.boot_services.?.getMemoryMap(&memory_map_size, memory_map, &memory_map_key, &memory_map_descriptor_size, &memory_map_descriptor_version)) {
        if (uefi.Status.Success != uefi.system_table.boot_services.?.allocatePool(uefi.tables.MemoryType.BootServicesData, memory_map_size, @ptrCast(&memory_map))) {
            return EarlyBootError.OutOfMemory;
        }
    }
    // Exit boot services
    if (uefi.Status.Success != uefi.system_table.boot_services.?.exitBootServices(uefi.handle, memory_map_key)) {
        while (true) {} // We must not return after the first call to exitBootServices
    }

    return UefiMemoryMapPointer{
        .address = @intFromPtr(memory_map),
        .count = @divExact(memory_map_size, memory_map_descriptor_size),
        .descriptor_size = memory_map_descriptor_size,
    };
}

// Allocator
const PageSlice = []allowzero align(assembly.page_size) assembly.Page;
const PagePointer = *allowzero align(assembly.page_size) assembly.Page;
const PagePointerMulti = [*]allowzero align(assembly.page_size) assembly.Page;

fn pagesContain(pages: PageSlice, page: PagePointer) bool {
    const pages_start = @intFromPtr(&pages.ptr[0]);
    const pages_end = @intFromPtr(&pages.ptr[pages.len]);
    const page_ = @intFromPtr(page);
    return pages_start <= page_ and page_ < pages_end;
}

fn bytesPages(bytes: []assembly.Byte) PageSlice {
    const first_page_number: usize = @divTrunc(@intFromPtr(bytes.ptr), assembly.page_size);
    const last_page_number: usize = @divTrunc(@intFromPtr(bytes.ptr + bytes.len) - 1, assembly.page_size);
    const page: PagePointerMulti = @ptrFromInt(first_page_number * assembly.page_size);
    const length: usize = last_page_number - first_page_number + 1;
    return page[0..length];
}

fn initAllocator(table: UefiMemoryMapPointer, code: PageSlice, stack: PagePointer) void {
    const table_pages = bytesPages(@as([*]assembly.Byte, @ptrFromInt(table.address))[0 .. table.count * table.descriptor_size]);

    var i: usize = 0;
    while (i < table.count) : (i += 1) {
        const entry = table.getEntry(i);
        if (entry.type == uefi.tables.MemoryType.ConventionalMemory) {
            const entry_: PageSlice = @as(PagePointerMulti, @ptrFromInt(entry.physical_start))[0..entry.number_of_pages];
            for (entry_) |*entry_page| {
                // Do not free the memory table pages as the memory table is still needed (e.g. to reclaim ACPI memory)
                if (!pagesContain(code, entry_page) and !pagesContain(table_pages, entry_page) and entry_page != stack) {
                    memory.free(entry_page);
                }
            }
        }
    }
}
