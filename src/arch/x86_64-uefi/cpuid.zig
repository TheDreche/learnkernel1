const std = @import("std");
const uefi = std.os.uefi;

const assembly = @import("asm.zig");
const dbg = @import("dbg.zig");

const Result = packed struct {
    eax: assembly.WordDouble,
    ebx: assembly.WordDouble,
    ecx: assembly.WordDouble,
    edx: assembly.WordDouble,
};

fn cpuid(in: u64, result: *Result) void {
    asm volatile (
        \\cpuid
        \\movl %eax, -0(%[out])
        \\movl %ebx, 4(%[out])
        \\movl %ecx, 8(%[out])
        \\movl %edx, 12(%[out])
        :
        : [in] "{rax}" (in),
          [out] "{rbp}" (result),
        : "rbx", "rcx", "rdx"
    );
}

// Detect specific features
const Vendor = enum {
    GenuineIntel,
    AuthenticAMD,
};

// Detect important features
pub const Features = struct {
    localApicId: u8,
    physicalAddressSize: u8,
};

pub const Error = error{
    UnknownVendor,
    UnsuccessfulFirmwareCall,
    MissingFeature,
    AddressPhysicalTooLong,
};

pub fn init(out: *uefi.protocol.SimpleTextOutput) Error!Features {
    var r: Result = undefined;
    cpuid(0, &r);
    const max_rax = r.eax;
    const vendor: Vendor = vendor: {
        const vendor_name = std.mem.toBytes(r.ebx) ++ std.mem.toBytes(r.edx) ++ std.mem.toBytes(r.ecx);
        if (std.mem.eql(assembly.Byte, &vendor_name, "GenuineIntel")) {
            break :vendor .GenuineIntel;
        } else if (std.mem.eql(assembly.Byte, &vendor_name, "AuthenticAMD")) {
            break :vendor .AuthenticAMD;
        } else return Error.UnknownVendor;
    };
    switch (vendor) {
        .AuthenticAMD => {
            if (uefi.Status.Success != out.outputString(&[_:0]u16{ 'D', 'e', 't', 'e', 'c', 't', 'e', 'd', ' ', 'A', 'M', 'D', ' ', 'C', 'P', 'U', '\r', '\n' })) {
                return Error.UnsuccessfulFirmwareCall;
            }
            if (max_rax < 0x0000_0001) {
                return Error.MissingFeature;
            }

            cpuid(0x8000_0000, &r);
            const max_rax_extended = r.eax;
            if (max_rax_extended < 0x8000_0001) {
                return Error.MissingFeature;
            }

            cpuid(0x0000_0001, &r);
            const localApicId: u8 = @intCast(r.ebx >> 24);
            if (0 == r.edx & 1 << 5) { // No MSR
                return Error.MissingFeature;
            }
            if (0 == r.edx & 1 << 9) { // No APIC
                return Error.MissingFeature;
            }

            cpuid(0x8000_0001, &r);
            if (0 == r.edx & 1 << 29) {
                // How can one even get this far without support for long mode?
                return Error.MissingFeature;
            }

            cpuid(0x8000_0008, &r);
            const physicalAddressSize: u8 = @truncate(r.eax);
            if (physicalAddressSize > assembly.address_physical_bits_max) {
                return Error.AddressPhysicalTooLong;
            }

            return Features{
                .localApicId = localApicId,
                .physicalAddressSize = physicalAddressSize,
            };
        },
        .GenuineIntel => {
            if (uefi.Status.Success != out.outputString(&[_:0]u16{ 'D', 'e', 't', 'e', 'c', 't', 'e', 'd', ' ', 'I', 'n', 't', 'e', 'l', ' ', 'C', 'P', 'U', '\r', '\n' })) {
                return Error.UnsuccessfulFirmwareCall;
            }
            if (max_rax < 0x0000_0001) {
                return Error.MissingFeature;
            }

            cpuid(0x8000_0000, &r);
            const max_rax_extended = r.eax;
            if (max_rax_extended < 0x8000_0001) {
                return Error.MissingFeature;
            }

            cpuid(0x0000_0001, &r);
            const localApicId: u8 = @intCast(r.ebx >> 24);
            if (0 == r.edx & 1 << 5) { // No MSR
                return Error.MissingFeature;
            }
            if (0 == r.edx & 1 << 9) { // No APIC
                return Error.MissingFeature;
            }

            cpuid(0x8000_0001, &r);
            if (0 == r.edx & 1 << 29) {
                // How can one even get this far without support for long mode?
                return Error.MissingFeature;
            }

            cpuid(0x8000_0008, &r);
            const physicalAddressSize: u8 = @truncate(r.eax);
            if (physicalAddressSize > assembly.address_physical_bits_max) {
                return Error.AddressPhysicalTooLong;
            }

            return Features{
                .localApicId = localApicId,
                .physicalAddressSize = physicalAddressSize,
            };
        },
    }
}

pub fn apicId() u8 {
    var r: Result = undefined;
    cpuid(0x0000_0001, &r);
    return @intCast(r.ebx >> 24);
}
