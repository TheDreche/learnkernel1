const dbg = @import("../../components/dbg.zig");

pub const Writer = dbg.Writer;
pub const writer = dbg.Writer;

pub const print = dbg.print;
pub const printf = dbg.printf;
pub const assert = dbg.assert;
pub const breakpoint = dbg.breakpoint;
