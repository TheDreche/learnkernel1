const Byte = @import("../../components/general.zig").Byte;
const port8 = @import("../../components/port8.zig");

pub const page_size = 1 << 12; // 4096

pub const PortByte = u16;

pub fn portByteRead(port: PortByte) Byte {
    return asm volatile ("inb %[port], %[result]"
        : [result] "={al}" (-> Byte),
        : [port] "N{dx}" (port),
    );
}

pub fn portByteWrite(port: PortByte, value: Byte) void {
    return asm volatile ("outb %[value], %[port]"
        :
        : [value] "{al}" (value),
          [port] "N{dx}" (port),
    );
}

const dbg__port = port8.Port{ .number = 0xE9 };
pub fn dbgPutChar(c: u8) void {
    dbg__port.write(c);
}

pub const Port32 = u16;

pub fn port32Read(port: Port32) u32 {
    return asm volatile ("inl %[port], %[result]"
        : [result] "={eax}" (-> u32),
        : [port] "N{dx}" (port),
    );
}

pub fn port32Write(port: Port32, value: u32) void {
    return asm volatile ("outl %[value], %[port]"
        :
        : [value] "{eax}" (value),
          [port] "N{dx}" (port),
    );
}
