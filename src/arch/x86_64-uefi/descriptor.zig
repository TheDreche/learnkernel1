pub const Privilege = u2;

// Null
pub const Null = packed struct {
    content: u64,

    pub fn init() Null {
        return Null{ .content = 0 };
    }
};

pub const NullLong = packed struct {
    content: u128,

    pub fn init() NullLong {
        return NullLong{ .content = 0 };
    }
};

// System Descriptors
const SystemDescriptorType = enum(u4) {
    ldt64 = 0x2,
    tss_available = 0x9,
    tss_busy = 0xB,
    call64 = 0xC,
    interrupt64 = 0xE,
    trap64 = 0xF,
};

// Not necessary as of now ... (but principally the same as Tss)
//pub const Ldt64 = packed struct {
//    pub fn init() Ldt64 {}
//};

pub const Tss = packed struct {
    limit_lo: u16,

    base_vlo: u16,

    base_lo: u8,
    segment_type: SystemDescriptorType,
    zero1: u1,
    privilege: Privilege,
    present: bool,

    limit_hi: u4,
    available: bool,
    zero2: u2,
    granularity: bool,
    base_hi: u8,

    base_vhi: u32,
    reserved1: u8,
    zero3: u8,
    reserved2: u16,

    pub fn init(address: *allowzero const anyopaque, limit: u20, privilege: Privilege, available: bool) Tss {
        const address_ = @intFromPtr(address);
        return Tss{
            .limit_lo = @truncate(limit >> 0),
            .base_vlo = @truncate(address_ >> 0),
            .base_lo = @truncate(address_ >> 16),
            .segment_type = SystemDescriptorType.tss_available,
            .zero1 = 0,
            .privilege = privilege,
            .present = true,
            .limit_hi = @truncate(limit >> 16),
            .available = available, // ignored, for system software
            .zero2 = 0,
            .granularity = false,
            .base_hi = @truncate(address_ >> 24),
            .base_vhi = @intCast(address_ >> 32),
            .reserved1 = 0,
            .zero3 = 0,
            .reserved2 = 0,
        };
    }
};

// Gates
pub const Call64 = packed struct {
    target_vlo: u16,
    selector: u16,
    reserved1: u8,

    system_descriptor_type: SystemDescriptorType,
    zero1: u1,
    privilege: Privilege,
    present: bool,

    target_lo: u16,
    target_hi: u32,

    reserved2: u8,
    zero2: u5,
    reserved3: u19,

    pub fn init(target: *allowzero const anyopaque, selector: u16, privilege: Privilege) Call64 {
        return Call64{
            .target_vlo = @truncate(target >> 0),
            .selector = selector,
            .reserved1 = 0,
            .system_descriptor_type = SystemDescriptorType.call64,
            .zero1 = 0,
            .privilege = privilege,
            .present = true,
            .target_lo = @truncate(target >> 16),
            .target_hi = @intCast(target >> 32),
            .reserved2 = 0,
            .zero2 = 0,
            .reserved3 = 0,
        };
    }
};

pub const Interrupt64 = packed struct {
    target_vlo: u16,
    selector: u16,
    ist: u3,
    zero1: u5,
    system_descriptor_type: SystemDescriptorType,
    zero2: u1,
    privilege: Privilege,
    present: bool,
    target_lo: u16,
    target_hi: u32,
    reserved: u32,

    pub fn init(target: *allowzero const anyopaque, selector: u16, ist: u3, privilege: Privilege) Interrupt64 {
        const target_ = @intFromPtr(target);
        return Interrupt64{
            .target_vlo = @truncate(target_ >> 0),
            .selector = selector,
            .ist = ist,
            .zero1 = 0,
            .system_descriptor_type = SystemDescriptorType.interrupt64,
            .zero2 = 0,
            .privilege = privilege,
            .present = true,
            .target_lo = @truncate(target_ >> 16),
            .target_hi = @intCast(target_ >> 32),
            .reserved = 0,
        };
    }
};

pub const Trap64 = packed struct {
    target_vlo: u16,
    selector: u16,
    ist: u3,
    zero1: u5,
    system_descriptor_type: SystemDescriptorType,
    zero2: u1,
    privilege: Privilege,
    present: bool,
    target_lo: u16,
    target_hi: u32,
    reserved: u32,

    pub fn init(target: *allowzero const anyopaque, selector: u16, ist: u3, privilege: Privilege) Interrupt64 {
        return Interrupt64{
            .target_vlo = @truncate(target >> 0),
            .selector = selector,
            .ist = ist,
            .zero1 = 0,
            .system_descriptor_type = SystemDescriptorType.interrupt64,
            .zero2 = 0,
            .privilege = privilege,
            .present = true,
            .target_lo = @truncate(target >> 16),
            .target_hi = @intCast(target >> 32),
            .reserved = 0,
        };
    }
};

// Segments
pub const SegmentCode = packed struct {
    limit_lo: u16,
    base_lo: u16,
    base_hi: u8,

    // Access byte
    accessed: bool,
    readable: bool,
    conforming: bool,
    executable: bool, // True
    segment_type: bool, // True
    privilege: Privilege,
    present: bool,

    limit_hi: u4,
    // Flags
    reserved: bool,
    long: bool, // True (32 bit: not supported)
    size: bool, // False (32 bit: not supported)
    granularity: bool, // Ignored (like most fields)

    base_vhi: u8,

    pub fn init(readable: bool, conforming: bool, privilege: Privilege) SegmentCode {
        return SegmentCode{
            .limit_lo = undefined,
            .base_lo = undefined,
            .base_hi = undefined,

            .accessed = true,
            .readable = readable,
            .conforming = conforming,
            .executable = true,
            .segment_type = true,
            .privilege = privilege,
            .present = true,

            .limit_hi = undefined,

            .reserved = undefined,
            .long = true,
            .size = false,
            .granularity = undefined,

            .base_vhi = undefined,
        };
    }
};

pub const SegmentData = packed struct {
    limit_lo: u16,
    base_lo: u16,
    base_hi: u8,

    // Access byte
    accessed: bool,
    writable: bool,
    direction: bool,
    executable: bool, // False
    segment_type: bool, // True
    privilege: Privilege,
    present: bool,

    limit_hi: u4,
    // Flags
    reserved: bool,
    long: bool, // True (32 bit: not supported)
    size: bool, // False (32 bit: not supported)
    granularity: bool, // Ignored (like most fields)

    base_vhi: u8,

    pub fn init(writable: bool, direction_down: bool, privilege: Privilege) SegmentData {
        return SegmentData{
            .limit_lo = undefined,
            .base_lo = undefined,
            .base_hi = undefined,

            .accessed = true,
            .writable = writable,
            .direction = direction_down,
            .executable = false,
            .segment_type = true,
            .privilege = privilege,
            .present = true,

            .limit_hi = undefined,

            .reserved = undefined,
            .long = true,
            .size = false,
            .granularity = undefined,

            .base_vhi = undefined,
        };
    }
};

// Descriptor groups
pub const IdtDescriptor = packed union {
    nullLong: NullLong,
    interrupt64: Interrupt64,
    trap64: Trap64,

    pub fn initNull() IdtDescriptor {
        return IdtDescriptor{ .nullLong = NullLong.init() };
    }
};
