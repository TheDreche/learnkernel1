const apic = @import("apic.zig");
const dbg = @import("dbg.zig");

pub fn noop() void {}

const IdtFrame = packed struct {
    rip: u64,
    cs: u64,
    rflags: u64,
    rsp: u64,
    ss: u64,
};

const Registers = packed struct {
    xmm15: f128,
    xmm14: f128,
    xmm13: f128,
    xmm12: f128,
    xmm11: f128,
    xmm10: f128,
    xmm9: f128,
    xmm8: f128,
    xmm7: f128,
    xmm6: f128,
    xmm5: f128,
    xmm4: f128,
    xmm3: f128,
    xmm2: f128,
    xmm1: f128,
    xmm0: f128,

    padding: u64,
    // Not available in 64-bit mode:
    // (but for me it is unclear why they don't need to be restored)
    //cs: u16, // Already within IdtFrame
    //ds: u16,
    //ss: u16, // Already within IdtFrame
    //es: u16,
    // TODO: Restore when necessary
    //fs: u16,
    //gs: u16,

    r15: u64,
    r14: u64,
    r13: u64,
    r12: u64,
    r11: u64,
    r10: u64,
    r9: u64,
    r8: u64,
    rbp: u64,
    //rsp: u64, // Already within IdtFrame
    rdi: u64,
    rsi: u64,
    rdx: u64,
    rcx: u64,
    rbx: u64,
    rax: u64,
};

const Error = u64;

export fn handleInterrupt8(frame: *IdtFrame, registers: *Registers, error_code: Error) noreturn {
    _ = registers;
    _ = frame;
    _ = error_code;
    dbg.print("Double fault!\n");
    dbg.breakpoint();
    while (true) {}
}

export fn handleInterrupt48(frame: *IdtFrame, registers: *Registers) void {
    dbg.print("Received timer interrupt 0x30 (48)\n");
    apic.eoi();
    _ = frame;
    _ = registers;
}

export fn handleInterrupt128(frame: *IdtFrame, registers: *Registers) void {
    dbg.print("Received interrupt 0x80 (128)\n");
    _ = frame;
    _ = registers;
}
