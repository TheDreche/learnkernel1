pub fn get(msr: u32) u64 {
    var result: u64 = undefined;
    asm volatile (
        \\rdmsr
        \\movl %eax, (%[out])
        \\movl %edx, 4(%[out])
        :
        : [in] "{ecx}" (msr),
          [out] "{rsi}" (&result),
        : "rax", "rdx"
    );
    return result;
}

pub fn set(msr: u32, value: u64) void {
    asm volatile ("wrmsr"
        :
        : [msr] "{ecx}" (msr),
          [lo] "{eax}" (@as(u32, @truncate(value))),
          [hi] "{edx}" (@as(u32, @intCast(value >> 32))),
    );
}
