pub fn ArrayPacked(typ: type, size: usize) type {
    if (size == 0) {
        return packed struct {
            pub fn getArray(self: @This()) [size]typ {
                _ = self;
                return [0]typ{};
            }
        };
    } else {
        return packed struct {
            item: typ,
            remainder: ArrayPacked(typ, size - 1),

            pub fn getArray(self: @This()) [size]typ {
                return [1]typ{self.item} ++ self.remainder.getArray();
            }
        };
    }
}
