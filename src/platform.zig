const config = @import("config");

const assembly = switch (config.target) {
    .x86_64_uefi => @import("arch/x86_64-uefi/asm.zig"),
};

pub const bits_per_byte = assembly.bits_per_byte;
pub const Byte = assembly.Byte;
pub const page_size = assembly.page_size;
pub const Page = assembly.Page;

pub const interface = switch (config.target) {
    .x86_64_uefi => @import("arch/x86_64-uefi/platform.zig"),
};
