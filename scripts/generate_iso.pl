#!/usr/bin/env perl
use v5.35;
use File::Copy;
use File::Temp qw/ tempdir /;
use Getopt::Long;
use IPC::Cmd qw/ can_run run /;

my $path_image = "";
my $path_output = "";

GetOptions(
	"image|img|i=s" => \$path_image,
	"output|out|o=s" => \$path_output,
);

my $path_xorriso = can_run("xorriso");

my $dir = tempdir(CLEANUP => 1);
copy($path_image, $dir . "/fat.img") or die();
run(command => [$path_xorriso, "-as", "mkisofs", "-R", "-e", "fat.img", "-no-emul-boot", "-o", $path_output, $dir]) or die();
