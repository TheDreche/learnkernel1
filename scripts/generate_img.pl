#!/usr/bin/env perl
use v5.35;
use Getopt::Long;
use IPC::Cmd qw/ can_run run /;

my $path_kernel = "";
my $path_output = "";

GetOptions(
	"kernel|k=s" => \$path_kernel,
	"output|out|o=s" => \$path_output,
);

my $path_dd = can_run("dd");
my $path_mformat = can_run("mformat");
my $path_mmd = can_run("mmd");
my $path_mcopy = can_run("mcopy");
my $path_xorriso = can_run("xorriso");

run(command => [$path_dd, "if=/dev/zero", "of=" . $path_output, "bs=1k", "count=1440"]) or die();
run(command => [$path_mformat, "-i", $path_output, "-f", "1440", "::"]) or die();
run(command => [$path_mmd, "-i", $path_output, "::/EFI"]) or die();
run(command => [$path_mmd, "-i", $path_output, "::/EFI/BOOT"]) or die();
run(command => [$path_mcopy, "-i", $path_output, $path_kernel, "::/EFI/BOOT/BOOTX64.EFI"]) or die();
