# Kernel Learning Project

This is my personal learning project where I try to learn how kernels work by
trying to write (a very simple and minimal) one.

## Hardware/… Support

For that reason, hardware support will be minimal: Once I understood how it
works, I don't need a lot of drivers anymore.

What I would like to learn:

- Reading from hard disks (FAT file system, as it is simple)
- How to deal with having multiple entry points (or: is this even required?)
  (GRUB, UEFI perhaps)
- User space
- How CPUs support swap
- Important aspects of driver interfaces (in general)
- Graphical output
- USB
- ACPI

## Architecture

The long plan for architecture is the following:

- Generally, design a monolithic kernel (because that requires the code to be in
  kernel space and is easier)
- Handle based permissions:
  - A handle is a number associated with something else in the kernel
  - A handle can be for:
    - File system access with specific permissions from specific CWD
    - A memory page
    - A process (allows killing and exchanging data)
    - A data exchange handle (every process can create such handles by providing
      an entry point which gets some data or by generating a mailbox instead
      which the application has to check regularly (blocking or nonblocking);
      loading an executable generates one as well, where passing data will
      result in a new process being spawned)
    - Data (to be used for exchanging data and handles securely)
- Handles needed for every process (for starting):
  - Request handle (whenever the process needs something, like a data exchange
    handle the parent can provide, or wants to give a data exchange handle for
    passing stdin data, the request should be sent through this handle)
  - Stdout handle (exchange handle where the output should be passed to,
    whenever something should be added to output)
  - Stderr handle (like stdout)
  - File system handle
- Starting a process generates a process handle

I am unsure about the file system:

- Should it be handle based?
- Should there only be one existing file system (provided by the kernel) and
  handles to access it from a specific user at a specific directory, or should
  every process have the ability to create its own “virtual” file system?

## Terminology

The terminology will be the usual Linux/Unix/…-terminology (init system, daemon,
kill, signal).
